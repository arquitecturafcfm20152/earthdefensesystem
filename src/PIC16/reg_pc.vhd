-- "reg_pc.vhd"

-- Implementa el PC y el hardware relacionado 

-- Las posibles operaciones son:
--   write_en = '1' : carga 8 bits de data_in y 5 del registro pclath;
--   push = '1'     : carga pc desde bus addr_in y desde pclath y guarda el anterior en el stack;
--   pop  = '1'     : carga el PC desde el stack;
--   load = '1'     : carga el PC desde addr_in y pclath ;
--   OTHERS         : incrementa el PC.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_unsigned.all;

entity reg_pc is
    port (
				clock_2     : in  STD_LOGIC;
				reset     	: in  STD_LOGIC;
				out_en    	: in  STD_LOGIC;
				write_en  	: in  STD_LOGIC;
				data_in   	: in  STD_LOGIC_VECTOR(7 downto 0);
--				from_pclath	: in  STD_LOGIC_VECTOR(4 downto 0);
				addr_in   	: in  STD_LOGIC_VECTOR(7 downto 0);
				push      	: in  STD_LOGIC;
				pop       	: in  STD_LOGIC;
				load      	: in  STD_LOGIC;
				stack_in  	: in  STD_LOGIC_VECTOR(7 downto 0);
				pc_skip		: in  STD_LOGIC;
				we		 	: out STD_LOGIC;
				stack_out 	: out STD_LOGIC_VECTOR(7 downto 0);
				data_out  	: out STD_LOGIC_VECTOR(7 downto 0);
				addr_out  	: out STD_LOGIC_VECTOR(7 downto 0);
				stack_ptr 	: out STD_LOGIC_VECTOR(2 downto 0)
         );
end reg_pc;

architecture mixed of reg_pc is

signal pc					: std_logic_vector (7 downto 0);
signal stack_ptr_aux		: std_logic_vector (2 downto 0);

begin

stack_ptr	<= stack_ptr_aux;
data_out 	<= pc when out_en = '1' else "ZZZZZZZZ"; -- bus A
addr_out 	<= pc;	  	-- hacia memoria de programa

process (clock_2, reset) -- pc_register
    begin
        if reset = '0' then
            pc <= "00000000";
			we	<= '0';
			stack_out <= (others => 'Z');
			stack_ptr_aux <= "000";		
        elsif rising_edge(clock_2) then

				if write_en = '1' then
				   pc 	<= data_in; -- desde bus de Resultados(alu)
--				   pc(12 downto 8) 	<= from_pclath;
				   we 				<= '0';
				   stack_out 		<= (others => 'Z');

				elsif push = '1' then
				   we 				<= '1';
 				   stack_out 		<= pc + 1;
				   stack_ptr_aux	<= stack_ptr_aux + 1;
				   pc			 	<= addr_in;	-- desde inst_8 de IR
--				   pc(12 downto 11) <= from_pclath(4 downto 3);

				elsif pop = '1' then
				 	we 				<= '0';
				 	pc 				<= stack_in;
					stack_ptr_aux 	<= stack_ptr_aux - 1;
					stack_out 		<= (others => 'Z');

				elsif load = '1' then
				    pc 			<= addr_in;
				 	we 			<= '0';
					stack_out 	<= (others => 'Z');
				
				elsif pc_skip = '1' then
					pc 			<= pc + 2;
					we 			<= '0';
					stack_out 	<= (others => 'Z');
				
				else
					pc 			<= pc + 1;
					we 			<= '0';
					stack_out 	<= (others => 'Z');
				end if;
        end if;
end process;

end mixed;
