--------------------------------------------------------------------------------
-- Company: EL-4102
-- Dev: Kenzo Lobos
--
-- Create Date:   20:13:27 11/08/2015
-- Design Name:   
-- Module Name:   C:/Users/K n z o/Documents/earthdefensesystem/src/PIC16/TestExample.vhd
-- Project Name:  PIC16
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: pic16
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TestbenchExample IS
END TestbenchExample;
 
ARCHITECTURE behavior OF TestbenchExample IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT pic16
    PORT(
	 		-- synthesis translate_off 
			c0 : OUT STD_LOGIC;
			c1 : OUT STD_LOGIC;
			c2 : OUT STD_LOGIC;
			c2a : OUT STD_LOGIC;
			c3 : OUT STD_LOGIC;
			c4 : OUT STD_LOGIC;
			inst : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
			program_counter : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
			-- synthesis translate_on 
         reset : IN  std_logic;
         clock50in : IN  std_logic;
         carry_out_centena : OUT  std_logic;
         wr_out : OUT  std_logic_vector(7 downto 0);
         bcd_port : OUT  std_logic_vector(15 downto 0);
         SF_D : OUT  std_logic_vector(11 downto 8);
         LCD_E : OUT  std_logic;
         LCD_RS : OUT  std_logic;
         LCD_RW : OUT  std_logic;
         SF_CE0 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal clock50in : std_logic := '0';

 	--Outputs
   signal carry_out_centena : std_logic;
   signal wr_out : std_logic_vector(7 downto 0);
   signal bcd_port : std_logic_vector(15 downto 0);
   signal SF_D : std_logic_vector(11 downto 8);
   signal LCD_E : std_logic;
   signal LCD_RS : std_logic;
   signal LCD_RW : std_logic;
   signal SF_CE0 : std_logic;
	
	signal c0 : STD_LOGIC;
	signal c1 : STD_LOGIC;
	signal c2 : STD_LOGIC;
	signal c2a : STD_LOGIC;
	signal c3 : STD_LOGIC;
	signal c4 : STD_LOGIC;
	signal inst : STD_LOGIC_VECTOR(13 DOWNTO 0);
	signal program_counter : STD_LOGIC_VECTOR(7 DOWNTO 0);

   -- Clock period definitions
   constant clock50in_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pic16 PORT MAP (
			 c0 => c0,
			 c1 => c1,
			 c2 => c2,
			 c2a => c2a,
			 c3 => c3,
			 c4 => c4,
			 inst => inst,
			 program_counter => program_counter,
          reset => reset,
          clock50in => clock50in,
          carry_out_centena => carry_out_centena,
          wr_out => wr_out,
          bcd_port => bcd_port,
          SF_D => SF_D,
          LCD_E => LCD_E,
          LCD_RS => LCD_RS,
          LCD_RW => LCD_RW,
          SF_CE0 => SF_CE0
        );

   -- Clock process definitions
   clock50in_process :process
   begin
		clock50in <= '0';
		wait for clock50in_period/2;
		clock50in <= '1';
		wait for clock50in_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 100ns;
		-- El PIC funciona mientras reset = 1
		reset <= '1';
		wait for 100 ms;
		reset <= '0';
      wait for clock50in_period*100;

      -- insert stimulus here 

      wait;
   end process;

END;
