LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity buf is
    PORT ( enable	: in std_logic;
		   bus_in	: in std_logic_vector (7 downto 0);
		   bus_out	: out std_logic_vector (7 downto 0)
         );
end buf;

ARCHITECTURE mixed OF buf IS

BEGIN

bus_out <= bus_in when enable = '1' else "ZZZZZZZZ";

END mixed;
 