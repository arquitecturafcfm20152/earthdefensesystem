-- Copyright (C) 1991-2009 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 9.0 Build 132 02/25/2009 SJ Full Version"
-- CREATED ON		"Mon Sep 28 18:40:34 2015"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY digit_s IS 
	PORT
	(
		clk :  IN  STD_LOGIC;
		rst :  IN  STD_LOGIC;
		from_wr_we :  IN  STD_LOGIC;
		eight_prl :  IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		modout :  OUT  STD_LOGIC;
		store :  OUT  STD_LOGIC;
		qdigit_c :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		qdigit_d :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		qdigit_u :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END digit_s;

ARCHITECTURE bdf_type OF digit_s IS 

COMPONENT digit
	PORT(Clock : IN STD_LOGIC;
		 Reset : IN STD_LOGIC;
		 Init : IN STD_LOGIC;
		 ModIn : IN STD_LOGIC;
		 ModOut : OUT STD_LOGIC;
		 Q : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT ctrl_serial
	PORT(reset : IN STD_LOGIC;
		 w_en : IN STD_LOGIC;
		 clock : IN STD_LOGIC;
		 eight_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 clock_o : OUT STD_LOGIC;
		 store_out : OUT STD_LOGIC;
		 serial_out : OUT STD_LOGIC;
		 init_out : OUT STD_LOGIC
	);
END COMPONENT;

SIGNAL	SYNTHESIZED_WIRE_9 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_10 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_5 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_8 :  STD_LOGIC;


BEGIN 



b2v_inst : digit
PORT MAP(Clock => SYNTHESIZED_WIRE_9,
		 Reset => rst,
		 Init => SYNTHESIZED_WIRE_10,
		 ModIn => SYNTHESIZED_WIRE_2,
		 ModOut => SYNTHESIZED_WIRE_5,
		 Q => qdigit_u);


b2v_inst1 : digit
PORT MAP(Clock => SYNTHESIZED_WIRE_9,
		 Reset => rst,
		 Init => SYNTHESIZED_WIRE_10,
		 ModIn => SYNTHESIZED_WIRE_5,
		 ModOut => SYNTHESIZED_WIRE_8,
		 Q => qdigit_d);


b2v_inst2 : ctrl_serial
PORT MAP(reset => rst,
		 w_en => from_wr_we,
		 clock => clk,
		 eight_in => eight_prl,
		 clock_o => SYNTHESIZED_WIRE_9,
		 store_out => store,
		 serial_out => SYNTHESIZED_WIRE_2,
		 init_out => SYNTHESIZED_WIRE_10);


b2v_inst3 : digit
PORT MAP(Clock => SYNTHESIZED_WIRE_9,
		 Reset => rst,
		 Init => SYNTHESIZED_WIRE_10,
		 ModIn => SYNTHESIZED_WIRE_8,
		 ModOut => modout,
		 Q => qdigit_c);


END bdf_type;