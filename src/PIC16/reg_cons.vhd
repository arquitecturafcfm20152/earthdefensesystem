-- "reg_cons.vhd"

-- Generador de constantes.
-- Genera un 0 para las instruccione CLR
-- y un 1 para las funciones INC/DEC.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY reg_cons IS
    PORT (
           out_en    : IN  STD_LOGIC;
           const_01  : IN  STD_LOGIC;
           data_out  : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
         );
END reg_cons;

ARCHITECTURE dataflow OF reg_cons IS
BEGIN
    data_out <= "000000000" & const_01 WHEN out_en = '1' ELSE "ZZZZZZZZZZ";
END dataflow;
