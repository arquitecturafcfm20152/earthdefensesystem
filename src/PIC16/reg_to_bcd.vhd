LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity reg_to_bcd is
    PORT ( clock     	: IN  STD_LOGIC;
		   rst		  	: IN  STD_LOGIC;
		   enable_in	: IN  STD_LOGIC;	
           unidad    	: IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		   decena		: IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		   centena		: IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		   to_bcd		: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
         );
end reg_to_bcd;

ARCHITECTURE mixed OF reg_to_bcd IS

SIGNAL valor		:STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL valor_aux	:STD_LOGIC_VECTOR(15 DOWNTO 0);

CONSTANT nada	: STD_LOGIC_VECTOR (6 DOWNTO 0)	 := "0000000";
CONSTANT cero	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1111110";
CONSTANT uno	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0110000";
CONSTANT dos	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1101101";
CONSTANT tres	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1111001";
CONSTANT cuatro	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0110011";
CONSTANT cinco	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1011011";
CONSTANT seis	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0011111";
CONSTANT siete	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1110000";
CONSTANT ocho	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1111111";
CONSTANT nueve	: STD_LOGIC_VECTOR (6 DOWNTO 0)  := "1110011";

BEGIN

to_bcd <= valor;

with unidad select
valor_aux(6 downto 0) <= 	cero	when "0000",
							uno		when "0001",
							dos		when "0010",
							tres	when "0011",
							cuatro	when "0100",
							cinco	when "0101",
							seis	when "0110",
							siete	when "0111",
							ocho	when "1000",
							nueve	when "1001",
							nada	when others;
			
with decena select
valor_aux(13 downto 7) <= 	cero	when "0000",
							uno		when "0001",
							dos		when "0010",
							tres	when "0011",
							cuatro	when "0100",
							cinco	when "0101",
							seis	when "0110",
							siete	when "0111",
							ocho	when "1000",
							nueve	when "1001",
							nada	when others;
			
valor_aux(15 downto 14) <= centena(1 downto 0);

PROCESS (clock, rst)
    BEGIN
    	  IF rst = '0' then
		  		valor <= "0000000000000000";
        ELSIF clock'EVENT AND clock = '1' THEN
            IF enable_in = '1' THEN
                valor <= valor_aux;
            END IF;
        END IF;
    END PROCESS;
END mixed;
