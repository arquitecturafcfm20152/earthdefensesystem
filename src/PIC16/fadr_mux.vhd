-- "fadr_mux.vhd"

-- Esta unidad implementa el modo de direccionamiento indirecto.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY fadr_mux IS
    PORT ( 	inst_7    : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
           	fsr       : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		 	bank_irp  : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
           	file_addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		 	data_addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0));
END fadr_mux;

ARCHITECTURE dataflow OF fadr_mux IS
BEGIN
    file_addr <= bank_irp(2) & fsr WHEN inst_7 = "0000000" ELSE	--cuando inst_7 es cero se apunta a INDF
                 bank_irp(1 downto 0) & inst_7;
    data_addr <= bank_irp(2) & fsr WHEN inst_7 = "0000000" ELSE
                 bank_irp(1 downto 0) & inst_7;
END dataflow;
