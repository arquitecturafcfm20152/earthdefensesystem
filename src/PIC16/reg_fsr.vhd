-- "reg_fsr.vhd"

-- FSR; registro de 8 bit con salida tri estado.
-- Usado en la unidad de control para el direccionamiento indirecto.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY reg_fsr IS
    PORT (
           clock_4     	: IN  STD_LOGIC;
		     rst		: IN  STD_LOGIC;
           out_en    	: IN  STD_LOGIC;
           write_en  	: IN  STD_LOGIC;
           data_in   	: IN  STD_LOGIC_VECTOR(7 DOWNTO 0);	   --bus_out
           data_out  	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);	   --bus_a
           fsr_out   	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
         );
END reg_fsr;

ARCHITECTURE mixed OF reg_fsr IS

SIGNAL valor: STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN

    data_out 		<= valor WHEN out_en = '1' ELSE "ZZZZZZZZ";
    fsr_out 		<= valor;

    do_it: PROCESS (clock_4, rst)
    BEGIN
    	  IF rst = '0' then
	   		valor <= "00000000";
        ELSIF clock_4'EVENT AND clock_4 = '1' THEN
            IF write_en = '1' THEN
                valor <= data_in;
            END IF;
        END IF;
    END PROCESS;
END mixed;
