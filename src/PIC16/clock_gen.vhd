library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_unsigned.all;

entity clock_gen is
    port (
				reset		: in  STD_LOGIC;
				clock		: in  STD_LOGIC;
				clock_l		: out STD_LOGIC;
				clock0		: out STD_LOGIC;
				clock1		: out STD_LOGIC;
				clock2		: out STD_LOGIC;
				clock2a		: out STD_LOGIC;
				clock3		: out STD_LOGIC;
				clock4		: out STD_LOGIC
         );
end clock_gen;

architecture mixed of clock_gen is

type estado_type is (no_clk, clk0, clk1, pause1, pause2, pause3, clk2, clk2a, clk3, clk4);
signal estado_r, estado_x	: estado_type;
signal clk0_aux, clk1_aux, clk2_aux, clk2a_aux, clk3_aux, clk4_aux : std_logic;
signal clock_lento: std_logic;
signal counter: std_logic_vector(23 downto 0);

begin

clock0 <= clk0_aux;
clock1 <= clk1_aux;
clock2 <= clk2_aux;
clock2a <= clk2a_aux;
clock3 <= clk3_aux;
clock4 <= clk4_aux;
clock_l <= clock_lento;

process (clock, reset)
	begin
		if reset = '0' then
			counter <= "000000000000000000000000";
			clock_lento <= '0';
		elsif rising_edge(clock)then
			counter <= counter + 1;
--			if counter = "101111101011110000100000" then
			if counter = "000000000000000000010100" then
				clock_lento <= not clock_lento;
				counter <= "000000000000000000000000";
			end if;
		end if;
end process;

process (clock_lento)
    begin
        if rising_edge(clock_lento) then
			estado_r <= estado_x;
		end if;
end process;

process (estado_r)
	begin
		case estado_r is
			when no_clk =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= clk0;
			when clk0 =>
				clk0_aux <= '1';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= clk1;
			when clk1 =>
				clk0_aux <= '0';
				clk1_aux <= '1';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= pause1;
			when pause1 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= pause2;
			when pause2 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= pause3;
			when pause3 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= clk2;
			when clk2 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '1';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= clk2a;
			when clk2a =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '1';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= clk3;
			when clk3 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '1';
				clk4_aux <= '0';
				estado_x <= clk4;
			when clk4 =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '1';
				estado_x <= clk0;
			when others =>
				clk0_aux <= '0';
				clk1_aux <= '0';
				clk2_aux <= '0';
				clk2a_aux <= '0';
				clk3_aux <= '0';
				clk4_aux <= '0';
				estado_x <= no_clk;
		end case;
end process;
end mixed;