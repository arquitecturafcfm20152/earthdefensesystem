-- "pic_alu.vhd"

-- Arithmetic-Logic Unit.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.std_logic_unsigned.all;

ENTITY pic_alu IS
  PORT ( operation : IN  STD_LOGIC_VECTOR(4 DOWNTO 0);
         a         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         b         : IN  STD_LOGIC_VECTOR(9 DOWNTO 0);
         res       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         carry_in  : IN  STD_LOGIC;
         carry_out : OUT STD_LOGIC;
         zero      : OUT STD_LOGIC;
		 match	   : OUT STD_LOGIC
       );
END pic_alu;

ARCHITECTURE dataflow OF pic_alu IS

CONSTANT  ALUOP_ADD        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00000";
CONSTANT  ALUOP_SUB_AB     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00001";
CONSTANT  ALUOP_SUB_BA	   : STD_LOGIC_VECTOR (4 DOWNTO 0)	:= "00010";
CONSTANT  ALUOP_AND        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00011";
CONSTANT  ALUOP_OR         : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00100";
CONSTANT  ALUOP_XOR        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00101";
CONSTANT  ALUOP_COM        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00110";
CONSTANT  ALUOP_ROR        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00111";
CONSTANT  ALUOP_ROL        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01000";
CONSTANT  ALUOP_SWAP       : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01001";
CONSTANT  ALUOP_BITCLR     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01010";
CONSTANT  ALUOP_BITSET     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01011";
CONSTANT  ALUOP_BITTESTCLR : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01100";
CONSTANT  ALUOP_BITTESTSET : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01101";
CONSTANT  ALUOP_PASSA      : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01110";
CONSTANT  ALUOP_PASSB      : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01111";

SIGNAL result 				: STD_LOGIC_VECTOR (8 DOWNTO 0);
SIGNAL bit_sel				: STD_LOGIC_VECTOR (2 DOWNTO 0);
SIGNAL bit_pattern 			: STD_LOGIC_VECTOR (7 DOWNTO 0);
SIGNAL bit_test    			: STD_LOGIC_VECTOR (7 DOWNTO 0);
SIGNAL m1, m2, m3			: STD_LOGIC;

BEGIN

bit_sel <= b(9 DOWNTO 7);

with bit_sel select
bit_pattern <= "00000001" WHEN "000",
               "00000010" WHEN "001",
               "00000100" WHEN "010",
               "00001000" WHEN "011",
               "00010000" WHEN "100",
               "00100000" WHEN "101",
               "01000000" WHEN "110",
               "10000000" WHEN "111",
			   "00000000" WHEN OTHERS;	
			
with operation select
result <= 	 ("0" & a) + b(7 DOWNTO 0)			 	WHEN ALUOP_ADD, 
			 ("0" & a) + NOT b(7 DOWNTO 0) + "1"	WHEN ALUOP_SUB_AB,
			 ("0" & b(7 DOWNTO 0)) + NOT a + "1" 	WHEN ALUOP_SUB_BA, 
			 "-" & (a AND b(7 DOWNTO 0))			WHEN ALUOP_AND,
			 "-" & (a OR b(7 DOWNTO 0))				WHEN ALUOP_OR, 
			 "-" & (a XOR b(7 DOWNTO 0))			WHEN ALUOP_XOR, 
			 "-" & (NOT a)							WHEN ALUOP_COM,
			 a(0) & carry_in & a(7 DOWNTO 1)		WHEN ALUOP_ROR,
			 a & carry_in 							WHEN ALUOP_ROL,
			 "-" & a(3 DOWNTO 0) & a(7 DOWNTO 4)	WHEN ALUOP_SWAP,
			 "-" & ((NOT bit_pattern) AND a)		WHEN ALUOP_BITCLR,
			 "-" & (bit_pattern OR a)				WHEN ALUOP_BITSET,
			 "-" & a 								WHEN ALUOP_PASSA,
			 "-" & b(7 DOWNTO 0) 					WHEN ALUOP_PASSB,
			 "ZZZZZZZZZ"							WHEN OTHERS;

res <= result(7 DOWNTO 0);
carry_out <= result(8);

bit_test <= bit_pattern AND a;

zero <= '1' WHEN result(7 DOWNTO 0) = "00000000" ELSE '0';

m1 <= '1' WHEN (bit_test /= "00000000") AND (operation = ALUOP_BITTESTSET) ELSE '0'; 
m2 <= '1' WHEN	(bit_test  = "00000000") AND (operation = ALUOP_BITTESTCLR) ELSE '0';
m3 <= '0' WHEN operation = "10000" ELSE '1';

match <= (m1 OR m2) AND m3;

END dataflow;
