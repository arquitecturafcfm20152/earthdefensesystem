-- "reg_w.vhd"

-- "Working" register.
-- Tiene dos salidas triestado para ambos buses.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity reg_w is
    PORT ( clock_4     	: IN  STD_LOGIC;
		   rst		  	: IN  STD_LOGIC;
           out_a_en   	: IN  STD_LOGIC;
           out_b_en   	: IN  STD_LOGIC;
           write_en   	: IN  STD_LOGIC;
		   --enable		: IN  STD_LOGIC;	
           data_in    	: IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		   valor_wr		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
           data_a_out 	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
           data_b_out 	: OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
         );
end reg_w;

ARCHITECTURE mixed OF reg_w IS

SIGNAL valor: STD_LOGIC_VECTOR(7 DOWNTO 0);
--SIGNAL valor2: STD_LOGIC_VECTOR(7 DOWNTO 0);
--SIGNAL clock_4_aux: STD_LOGIC;

BEGIN
	--clock_4_aux <= clock_4;
    data_a_out <= valor 		WHEN out_a_en = '1' ELSE "ZZZZZZZZ"; 
    data_b_out <= "00" & valor 	WHEN out_b_en = '1' ELSE "ZZZZZZZZZZ"; 
	valor_wr <= valor;

PROCESS (clock_4, rst)
    BEGIN
    	  IF rst = '0' then
		  		valor <= "00000000";
        ELSIF clock_4'EVENT AND clock_4 = '1' THEN
            IF write_en = '1' THEN
                valor <= data_in;
            END IF;
        END IF;
    END PROCESS;
END mixed;
