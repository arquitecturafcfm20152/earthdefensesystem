library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY pclath IS
    PORT (
           clock_4     : IN  STD_LOGIC;
		   reset	 : IN  STD_LOGIC;
           out_en    : IN  STD_LOGIC;
           write_en  : IN  STD_LOGIC;
           data_in   : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
           data_out  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
--		 	  to_pc  : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
         );
END pclath;

ARCHITECTURE mixed OF pclath IS

SIGNAL valor: STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN

    data_out 	<= valor WHEN out_en = '1' ELSE "ZZZZZZZZ";
--    to_pc 		<= valor(4 downto 0);

    do_it: PROCESS (clock_4, reset)
    BEGIN
	   IF reset = '0' then
	   	  valor <= "00000000";
        ELSIF clock_4'EVENT AND clock_4 = '1' THEN
            IF write_en = '1' THEN
                valor <= data_in;
            END IF;
        END IF;
    END PROCESS;
END mixed;