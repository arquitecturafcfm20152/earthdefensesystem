library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity stack_reg is

Port ( clock_3 		: in std_logic;
	   reset		: in std_logic;
	   we 			: in std_logic;
       write_data 	: in std_logic_vector(7 downto 0);
	   address 		: in std_logic_vector(2 downto 0);
       read_data 	: out std_logic_vector(7 downto 0)
);
end stack_reg;

architecture Behavioral of stack_reg is

type mem is array (7 downto 0) of std_logic_vector(7 downto 0);
signal memoria: mem;
signal add: natural range 0 to 7;

begin

add <= conv_integer(address);

process(clock_3, reset)

	begin
		if reset = '0' then 
			for i in 7 downto 0 loop
				memoria(i) <= "00000000";
			end loop;
		elsif clock_3'event and clock_3 = '1' then
			if we = '1' then
				memoria(add) <= write_data;
			end if;
		end if;

end process;

read_data <= memoria(add);

end Behavioral;
