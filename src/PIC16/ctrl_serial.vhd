library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_unsigned.all;

entity ctrl_serial is
    port (
				reset		: in  STD_LOGIC;
				w_en		: in  STD_LOGIC;				
				clock		: in  STD_LOGIC;
				eight_in	: in  STD_LOGIC_VECTOR(7 downto 0);
				clock_o		: out STD_LOGIC;
				store_out	: out STD_LOGIC;				
				serial_out	: out STD_LOGIC;
				init_out	: out STD_LOGIC
         );
end ctrl_serial;

architecture mixed of ctrl_serial is

type estado_type is (init_up, serial, store, stop, off);
signal estado_r, estado_x	: estado_type;
signal counter: std_logic_vector(2 downto 0);
signal pointer: integer range 0 to 3;

begin

pointer <= conv_integer(counter);
serial_out <= eight_in(pointer) when estado_r /= off else '0';
clock_o <= clock when estado_r /= off else '0';

process (clock, reset, estado_x)
	begin
		if reset = '0' then	
			counter <= "111";
			estado_r <= off;
		elsif rising_edge(clock)then
			estado_r <= estado_x;
			if estado_x = serial then
				counter <= counter - 1;
			else 
				counter <= "111";
			end if;
		end if;
end process;

process (estado_r, counter, w_en)
	begin
		case estado_r is
			when init_up =>
				init_out <= '1';
				estado_x <= serial;
				store_out <= '0';
			when serial =>
				init_out <= '0';
				store_out <= '0';
				if counter = "000" then 
					estado_x <= store;
				else
					estado_x <= serial;
				end if;
			when store =>
				store_out <= '1';
				init_out <= '0';
				estado_x <= stop;
			when stop =>
				store_out <= '0';
				init_out <= '0';
				if w_en = '0' then
					estado_x <= off;
				else
					estado_x <= stop;
				end if;
			when off =>
				store_out <= '0';
				init_out <= '0';
				if w_en = '1' then
					estado_x <= init_up;
				else
					estado_x <= off;
				end if;			
			when others =>
		end case;
	end process;
end mixed;