-- "reg_inst.vhd"       

-- Instruction register.  
-- 
-- Este registro guarda la instrucci�n que se est� ejecutando
-- Si skip = '0' una nueva instrucci�n es cargada del bus de instrucs.
-- Si skip = '1' se produce una NOP, lo que sirve para las intrucciones
-- de dos ciclos.
-- imm_out es una salida triestado para operandos inmediatos que va en el bus B.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY reg_inst IS

    PORT (
				clock_1     : IN  STD_LOGIC;
				reset     	: IN  STD_LOGIC;
				--skip      	: IN  STD_LOGIC;
				out_en    	: IN  STD_LOGIC;
				inst_in   	: IN  STD_LOGIC_VECTOR(13 DOWNTO 0);	
				inst_out  	: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
				inst_7    	: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
				inst_8   	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
				imm_out   	: OUT STD_LOGIC_VECTOR(9 DOWNTO 0) --bus b
);
END reg_inst;

ARCHITECTURE mixed OF reg_inst IS
SIGNAL valor: STD_LOGIC_VECTOR(13 DOWNTO 0);
BEGIN

imm_out <= valor(9 DOWNTO 0) WHEN out_en = '1' ELSE "ZZZZZZZZZZ";

inst_out 	<= valor;					-- al prgm_ctrl
inst_7 		<= valor(6 DOWNTO 0);		-- al fadr_mux y luego al prgm_ctrl
inst_8 		<= valor(7 DOWNTO 0); 		-- al PC

    do_it: PROCESS (clock_1, reset)
    BEGIN
        IF reset = '0' THEN
            	valor <= "00000000000000"; --  NOP.
        ELSIF clock_1'EVENT AND clock_1 = '1' THEN
				valor <= inst_in;
        END IF;
    END PROCESS;
END mixed;