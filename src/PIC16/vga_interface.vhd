----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:39:24 11/19/2015 
-- Design Name: 
-- Module Name:    vgaInterface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vgaInterface is
    Port ( clock : in  STD_LOGIC;
           instruction : in STD_LOGIC_VECTOR (13 downto 0);
           char : out  STD_LOGIC_VECTOR (7 downto 0));
end vgaInterface;

architecture Behavioral of vgaInterface is

signal charIn: STD_LOGIC_VECTOR(7 DOWNTO 0);
signal code : STD_LOGIC_VECTOR(5 DOWNTO 0);

begin

	code   <= instruction(13 downto 8);
	charIn <= instruction(7 downto 0);
	
	--process(clock)
	--begin
	--if clock'event and clock = '1' and code = "110011" then
	--	char <= charIn;
	--else
	--	char <= "00000000";
	--end if;
	--end process;
	
	process (clock)   
	begin
	if (rising_edge(clock)) then
		if (code = "110011") then
			char <= charIn;
		else
			char <= "00000000";
		end if;
	end if;
	end process;
	

end Behavioral;