-- "pic_ctrl.vhd"

-- controlador y decodificador de instruciones.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY pic_ctrl IS
    PORT ( inst        : IN  STD_LOGIC_VECTOR(13 DOWNTO 0);
           file_addr   : IN  STD_LOGIC_VECTOR(8 DOWNTO 0);
		   match	   : IN  STD_LOGIC;
           alu_op      : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
           w_wen       : OUT STD_LOGIC;
           w_a_oen     : OUT STD_LOGIC;
           w_b_oen     : OUT STD_LOGIC;
           pc_wen      : OUT STD_LOGIC;
           pc_oen      : OUT STD_LOGIC;
		   pclath_wen  : OUT STD_LOGIC;
		   pclath_oen  : OUT STD_LOGIC;
           pc_push     : OUT STD_LOGIC;
           pc_pop      : OUT STD_LOGIC;
           pc_load     : OUT STD_LOGIC;
           fsr_wen     : OUT STD_LOGIC;
           fsr_oen     : OUT STD_LOGIC;
		   file_wen    : OUT STD_LOGIC;
           file_oen    : OUT STD_LOGIC;
		   pc_skip	   : OUT STD_LOGIC;
           imm_oen     : OUT STD_LOGIC;
           status_oen  : OUT STD_LOGIC;
           status_wen  : OUT STD_LOGIC;
           carry_wen   : OUT STD_LOGIC;
           zero_wen    : OUT STD_LOGIC;
           const_oen   : OUT STD_LOGIC;
           const_01    : OUT STD_LOGIC
			  );
END ENTITY pic_ctrl;

ARCHITECTURE mixed OF pic_ctrl IS

CONSTANT  ALUOP_ADD        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00000";
CONSTANT  ALUOP_SUB_AB     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00001";
CONSTANT  ALUOP_SUB_BA	   : STD_LOGIC_VECTOR (4 DOWNTO 0)	:= "00010";
CONSTANT  ALUOP_AND        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00011";
CONSTANT  ALUOP_OR         : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00100";
CONSTANT  ALUOP_XOR        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00101";
CONSTANT  ALUOP_COM        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00110";
CONSTANT  ALUOP_ROR        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "00111";
CONSTANT  ALUOP_ROL        : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01000";
CONSTANT  ALUOP_SWAP       : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01001";
CONSTANT  ALUOP_BITCLR     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01010";
CONSTANT  ALUOP_BITSET     : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01011";
CONSTANT  ALUOP_BITTESTCLR : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01100";
CONSTANT  ALUOP_BITTESTSET : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01101";
CONSTANT  ALUOP_PASSA      : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01110";
CONSTANT  ALUOP_PASSB      : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "01111";

CONSTANT ADDWF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000111";
CONSTANT ANDWF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000101";
CONSTANT CLRF   : STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0000011";
CONSTANT CLRW   : STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0000010";
CONSTANT COMF   : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001001";
CONSTANT DECF   : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000011";
CONSTANT DECFSZ : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001011";
CONSTANT INCF   : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001010";
CONSTANT INCFSZ : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001111";
CONSTANT IORWF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000100";
CONSTANT MOVF   : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001000";
CONSTANT MOVWF  : STD_LOGIC_VECTOR (6 DOWNTO 0)  := "0000001";
CONSTANT NOP    : STD_LOGIC_VECTOR (13 DOWNTO 0) := "00000000000000";
CONSTANT RLF    : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001101";
CONSTANT RRF    : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001100";
CONSTANT SUBWF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000010";
CONSTANT SWAPF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "001110";
CONSTANT XORWF  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "000110";

CONSTANT BCF    : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "0100";
CONSTANT BSF    : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "0101";
CONSTANT BTFSC  : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "0110";
CONSTANT BTFSS  : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "0111";

CONSTANT ADDLW  : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "11111"; 
CONSTANT ANDLW  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "111001";
CONSTANT CALL   : STD_LOGIC_VECTOR (2 DOWNTO 0)  := "100";
--CLRWDT NO IMPLEMENTADO
CONSTANT GOTO   : STD_LOGIC_VECTOR (2 DOWNTO 0)  := "101";
CONSTANT IORLW  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "111000";
CONSTANT MOVLW  : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "1100";
--RETFIE NO IMPLEMENTADO
CONSTANT RETLW  : STD_LOGIC_VECTOR (3 DOWNTO 0)  := "1101";
CONSTANT RETUR  : STD_LOGIC_VECTOR (10 DOWNTO 0) := "00000000001";
--SLEEP NO IMPLEMENTADO
CONSTANT SUBLW  : STD_LOGIC_VECTOR (4 DOWNTO 0)  := "11110";
CONSTANT XORLW  : STD_LOGIC_VECTOR (5 DOWNTO 0)  := "111010";

-- Master output y write-enable para la memoria de datos.
SIGNAL file_moen : STD_LOGIC;
SIGNAL file_mwen : STD_LOGIC;

-- selector de destino ('0' = W, '1' = FILE).
SIGNAL dest : STD_LOGIC;

BEGIN

    dest <= inst(7);
    
    -- Este proceso genera las se�ales write- y output-enable 
    -- para todos los registros o la memoria de datos.

    register_select: PROCESS (file_moen, file_mwen, file_addr)
    BEGIN
        pc_oen     <= '0';
        status_oen <= '0';
        fsr_oen    <= '0';
        file_oen   <= '0';
        pc_wen     <= '0';
        status_wen <= '0';
        fsr_wen    <= '0';
        file_wen   <= '0';
		pclath_oen <= '0';
		pclath_wen <= '0';

        
        IF file_moen = '1' THEN
            CASE file_addr IS
				WHEN "000000010" => pc_oen	  	<= '1';
				WHEN "010000010" => pc_oen	  	<= '1';
          	 	WHEN "100000010" => pc_oen	  	<= '1';
			 	WHEN "110000010" => pc_oen	  	<= '1';
				WHEN "000000011" => status_oen 	<= '1';
				WHEN "010000011" => status_oen 	<= '1';
				WHEN "100000011" => status_oen 	<= '1';
				WHEN "110000011" => status_oen 	<= '1';
				WHEN "000000100" => fsr_oen    	<= '1';
			 	WHEN "010000100" => fsr_oen    	<= '1';
				WHEN "100000100" => fsr_oen    	<= '1';
				WHEN "110000100" => fsr_oen    	<= '1';
			 	WHEN "000001010" => pclath_oen 	<= '1';
			 	WHEN "010001010" => pclath_oen 	<= '1';
			 	WHEN "100001010" => pclath_oen 	<= '1';
			 	WHEN "110001010" => pclath_oen 	<= '1';
				WHEN OTHERS  	 => file_oen   	<= '1';
            END CASE;
        END IF;

        IF file_mwen = '1' THEN
            CASE file_addr IS
             	WHEN "000000010" => pc_wen	  	<= '1';
		 		WHEN "010000010" => pc_wen	  	<= '1';
       	 		WHEN "100000010" => pc_wen	  	<= '1';
		 		WHEN "110000010" => pc_wen	  	<= '1';
             	WHEN "000000011" => status_wen 	<= '1';
             	WHEN "010000011" => status_wen 	<= '1';
             	WHEN "100000011" => status_wen 	<= '1';
             	WHEN "110000011" => status_wen 	<= '1';
             	WHEN "000000100" => fsr_wen    	<= '1';
		 		WHEN "010000100" => fsr_wen    	<= '1';
             	WHEN "100000100" => fsr_wen    	<= '1';
             	WHEN "110000100" => fsr_wen    	<= '1';
				WHEN "000001010" => pclath_wen 	<= '1';
				WHEN "010001010" => pclath_wen 	<= '1';
				WHEN "100001010" => pclath_wen 	<= '1';
				WHEN "110001010" => pclath_wen 	<= '1';
             	WHEN OTHERS  	 => file_wen   	<= '1';
            END CASE;
        END IF;
    END PROCESS;

    -- Este es el proceso que genera las se�ales de control. 

    main: PROCESS (inst, dest, match)
    BEGIN

-- Se asignan los valores por defecto para las se�ales de control.

        w_wen      <= '0';
        w_a_oen    <= '0';
        w_b_oen    <= '0';
        pc_push    <= '0';
        pc_pop     <= '0';
        pc_load    <= '0';
		pc_skip	   <= '0';
        imm_oen    <= '0';
        carry_wen  <= '0';
        zero_wen   <= '0';
        const_oen  <= '0';
        const_01   <= '0';
        file_moen  <= '0';
        file_mwen  <= '0';
        alu_op     <= "10000";

        IF inst = NOP THEN
        --NOP.
        ELSIF inst(13 DOWNTO 8) = ADDWF  THEN
            w_b_oen 	<= '1';                -- W en el bus B.
            file_moen 	<= '1';                -- Registro en el bus A.
            alu_op 		<= ALUOP_ADD;          -- Suma.
            carry_wen 	<= '1';                -- Renueva el flag de Carry.
            zero_wen 	<= '1';                -- Renueva el flag de Zero.
            IF dest = '0' THEN
                w_wen 	<= '1';                -- Guarda el resultado en W.      
            ELSE
				file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
        ELSIF inst(13 DOWNTO 8) = ANDWF  THEN
            w_b_oen 	<= '1';                -- W en el bus B.
            file_moen 	<= '1';                -- Registro en el bus A.
            alu_op 		<= ALUOP_AND;          -- AND.
            zero_wen 	<= '1';                -- Renueva el flag de Zero.
            IF dest = '0' THEN
                w_wen 	<= '1';                -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
        ELSIF inst(13 DOWNTO 7) = CLRF   THEN       
		  	const_oen 	<= '1'; 
            const_01  	<= '0';				   -- "0" en el bus B.     
            alu_op 		<= ALUOP_PASSB;        -- Copia B en la salida.
            file_mwen 	<= '1';                -- Guarda en el registro.            
            zero_wen 	<= '1';                -- Renueva el flag de Zero.
        ELSIF inst(13 DOWNTO 7) = CLRW   THEN
            const_oen <= '1';
            const_01  <= '0';                  -- "0" en el bus B.
            alu_op <= ALUOP_PASSB;             -- Copia B en la salida.
            w_wen <= '1';                      -- Lo guarda en el registro.
            zero_wen <= '1';                   -- Renueva el flag de Zero.
        ELSIF inst(13 DOWNTO 8) = COMF   THEN
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_COM;               -- Invierte.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
            zero_wen <= '1';                   -- Renueva el flag de Zero.
        ELSIF inst(13 DOWNTO 8) = DECF   THEN
            file_moen <= '1';                  -- Registro en el bus A.
            const_oen <= '1';
            const_01  <= '1';                  -- "0000000001" en el bus B.
            alu_op <= ALUOP_SUB_AB;            -- Resta.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
            zero_wen <= '1';                   -- Renueva el flag Zero.
        ELSIF inst(13 DOWNTO 8) = DECFSZ THEN
            file_moen <= '1';                  -- Registro en el bus A.
            const_oen <= '1';
            const_01  <= '1';                  -- "0000000001" en el bus B.
            alu_op <= ALUOP_SUB_AB;            -- Resta.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en el W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
            IF match = '1' THEN
			    pc_skip		<= '1';			   -- Salta a la nueva instrucion si match es '1'.
            END IF;
        ELSIF inst(13 DOWNTO 8) = INCF   THEN
            file_moen <= '1';                  -- Registro en el bus A.
            const_oen <= '1';
            const_01  <= '1';                  -- "0000000001" en el bus B.
            alu_op <= ALUOP_ADD;               -- Suma.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
            zero_wen <= '1';                   -- Renueva el flag Zero.
        ELSIF inst(13 DOWNTO 8) = INCFSZ THEN
            file_moen <= '1';                  -- Registro en el bus A.
            const_oen <= '1';
            const_01  <= '1';                  -- "0000000001" en el bus B.
            alu_op <= ALUOP_ADD;               -- Suma.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
            IF match = '1' THEN           
				 pc_skip		<= '1';		   -- Salta a la proxima instruccion si match es '1'.
            END IF;
        ELSIF inst(13 DOWNTO 8) = IORWF  THEN
            w_b_oen <= '1';                    -- W en el bus B.
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_OR;                -- OR.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
        ELSIF inst(13 DOWNTO 8) = MOVF   THEN
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_PASSA;             -- Copia A a la salida.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;
        ELSIF inst(13 DOWNTO 7) = MOVWF  THEN
            w_a_oen <= '1';                    -- W en el bus A.
            alu_op <= ALUOP_PASSA;             -- Copia A en la salida.
            file_mwen <= '1';                  -- Guarda el resultado en el registro.
        ELSIF inst(13 DOWNTO 8) = RLF    THEN
            file_moen <= '1';                  -- Registro en en bus A.
            alu_op <= ALUOP_ROL;               -- Shift a la izquierda.
            carry_wen <= '1';                  -- Renueva el flag Carry.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;
        ELSIF inst(13 DOWNTO 8) = RRF    THEN
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_ROR;               -- Shift a la derecha.
            carry_wen <= '1';                  -- Renueva el flag Carry.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;
        ELSIF inst(13 DOWNTO 8) = SUBWF  THEN
            w_b_oen <= '1';                    -- W en el bus B.
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_SUB_AB;            -- Resta.
            carry_wen <= '1';                  -- Renueva el flag Carry.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
        ELSIF inst(13 DOWNTO 8) = SWAPF  THEN
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_SWAP;              -- Intercambia los 4 bits msb por 4 lsb.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;
        ELSIF inst(13 DOWNTO 8) = XORWF  THEN
            w_b_oen <= '1';                    -- W en el bus B.
            file_moen <= '1';                  -- Registro en el bus A.
            alu_op <= ALUOP_XOR;               -- XOR.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            IF dest = '0' THEN
                w_wen <= '1';                  -- Guarda el resultado en W.      
            ELSE
                file_mwen <= '1';              -- Guarda el resultado en el registro.
            END IF;                          
        ELSIF inst(13 DOWNTO 10) = BCF    THEN
            file_moen <= '1';                  -- Registro en el bus A.
            imm_oen <= '1';                    -- IR(9 a 0) en el bus B.
            alu_op <= ALUOP_BITCLR;            -- borra el bit seleccionado.
            file_mwen <= '1';                  -- Guarda el resultado en el registro.
        ELSIF inst(13 DOWNTO 10) = BSF    THEN
            file_moen <= '1';                  -- Registro en el bus A.
            imm_oen <= '1';                    -- IR(9 a 0) en el bus B.
            alu_op <= ALUOP_BITSET;            -- Fija el valor de un bit.
            file_mwen <= '1';                  -- Guarda el resultado en el registro.
        ELSIF inst(13 DOWNTO 10) = BTFSC  THEN
            file_moen <= '1';                  -- Registro en el bus A.
            imm_oen <= '1';                    -- IR(9 a 0) en el bus B.
            alu_op <= ALUOP_BITTESTCLR;        -- Testea si el bit est� borrado.
            IF match = '1' THEN             
			    pc_skip		<= '1';			  -- Salta a la proxima instruccion si match es '1'.
            END IF;
        ELSIF inst(13 DOWNTO 10) = BTFSS  THEN
            file_moen <= '1';                  -- Registro en el bus A.
            imm_oen <= '1';                    -- IR(9 a 0) en el bus B.
            alu_op <= ALUOP_BITTESTSET;        -- Testea si el bit est� en uno.
            IF match = '1' THEN
				pc_skip		<= '1';			    -- Salta a la proxima instruccion si match es '1'.
            END IF;
        ELSIF inst(13 DOWNTO 9) = ADDLW  THEN  
            w_a_oen <= '1';                    -- W en el bus A.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_ADD;               -- Suma.
		  	carry_wen <= '1';                  -- Renueva flag Carry.
            zero_wen <= '1';                   -- Renueva flag Zero.
            w_wen <= '1';                      -- Guarda resultado en W.  				
	    ELSIF inst(13 DOWNTO 8) = ANDLW  THEN
            w_a_oen <= '1';                    -- W en el bus A.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_AND;               -- AND.
            zero_wen <= '1';                   -- Renueva flag Zero.
            w_wen <= '1';                      -- Guarda resultado en W.  
    	ELSIF inst(13 DOWNTO 11) = CALL  THEN
            pc_push <= '1';                    -- Push en el stack y carga nuevo valor del PC.
        ELSIF inst(13 DOWNTO 11) = GOTO  THEN
            pc_load <= '1';					   -- Carga valor del stack al PC
        ELSIF inst(13 DOWNTO 8) = IORLW  THEN
            w_a_oen <= '1';                    -- W en el bus A.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_OR;                -- OR.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            w_wen <= '1';                      -- Guarda el resultado en W.
        ELSIF inst(13 DOWNTO 10) = MOVLW THEN
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_PASSB;             -- Copia B a la salida.
            w_wen <= '1';                      -- Guarda el resultado en W.
        ELSIF inst(13 DOWNTO 10) = RETLW THEN
            pc_pop <= '1';                     -- Pop al PC desde el stack.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_PASSB;             -- Copia B a la salida.
            w_wen <= '1';                      -- Guarda el resultado en W.      
	    ELSIF inst(13 downto 3) = RETUR THEN
  		  	pc_pop <= '1';				  	   -- Pop al PC desde el stack.
	    ELSIF inst(13 DOWNTO 9) = SUBLW  THEN 
            w_a_oen <= '1';                    -- W en el bus A.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_SUB_BA;            -- Resta.
            carry_wen <= '1';                  -- Renueva el flag Carry.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            w_wen <= '1';                      -- Guarda el resultado en W.     		
        ELSIF inst(13 DOWNTO 8) = XORLW  THEN
            w_a_oen <= '1';                    -- W en el bus A.
            imm_oen <= '1';                    -- Literal en el bus B.
            alu_op <= ALUOP_XOR;               -- XOR.
            zero_wen <= '1';                   -- Renueva el flag Zero.
            w_wen <= '1';                      -- Guarda el resultado en W.    
        END IF;
    END PROCESS;
END ARCHITECTURE mixed;

