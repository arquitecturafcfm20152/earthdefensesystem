-- Copyright (C) 1991-2009 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 9.0 Build 132 02/25/2009 SJ Full Version"
-- CREATED ON		"Mon Sep 28 08:52:33 2015"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY pic16 IS 
	PORT
	(
	 	-- synthesis translate_off 
		c0 : OUT STD_LOGIC;
		c1 : OUT STD_LOGIC;
		c2 : OUT STD_LOGIC;
		c2a : OUT STD_LOGIC;
		c3 : OUT STD_LOGIC;
		c4 : OUT STD_LOGIC;
		inst : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		program_counter : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		-- synthesis translate_on 		
		reset :  IN  STD_LOGIC;
		clock50in :  IN  STD_LOGIC;
		carry_out_centena :  OUT  STD_LOGIC;
		wr_out : OUT STD_LOGIC_VECTOR(7 downto 0);
		bcd_port :  OUT  STD_LOGIC_VECTOR(15 DOWNTO 0);
		SF_D   : out std_logic_vector(11 downto 8);
      LCD_E  : out std_logic; 
      LCD_RS : out std_logic; 
      LCD_RW : out std_logic;
      SF_CE0 : out std_logic
	);
END pic16;

ARCHITECTURE bdf_type OF pic16 IS 

COMPONENT lcd
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		test_lcd : IN std_logic_vector(13 downto 0);
		test_lcd_wr : IN std_logic_vector(7 downto 0);          
		SF_D : OUT std_logic_vector(11 downto 8);
		LCD_E : OUT std_logic;
		LCD_RS : OUT std_logic;
		LCD_RW : OUT std_logic;
		SF_CE0 : OUT std_logic
		);
	END COMPONENT;

COMPONENT scale_clock
	PORT(
		clk_50Mhz : IN std_logic;
		rst : IN std_logic;          
		clk_div : OUT std_logic
		);
	END COMPONENT;

COMPONENT fadr_mux
	PORT(bank_irp : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 fsr : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 inst_7 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
		 data_addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		 file_addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
	);
END COMPONENT;

COMPONENT pclath
	PORT(clock_4 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 out_en : IN STD_LOGIC;
		 write_en : IN STD_LOGIC;
		 data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT stack_reg
	PORT(clock_3 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 we : IN STD_LOGIC;
		 address : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 write_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 read_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT memoria_programa
	PORT(we : IN STD_LOGIC;
		 inclock : IN STD_LOGIC;
		 outclock : IN STD_LOGIC;
		 address : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
		 q : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
	);
END COMPONENT;

COMPONENT digit_s
	PORT(rst : IN STD_LOGIC;
		 from_wr_we : IN STD_LOGIC;
		 clk : IN STD_LOGIC;
		 eight_prl : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 modout : OUT STD_LOGIC;
		 store : OUT STD_LOGIC;
		 qdigit_c : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 qdigit_d : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 qdigit_u : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT clock_gen
	PORT(reset : IN STD_LOGIC;
		 clock : IN STD_LOGIC;
		 clock_l : OUT STD_LOGIC;
		 clock0 : OUT STD_LOGIC;
		 clock1 : OUT STD_LOGIC;
		 clock2 : OUT STD_LOGIC;
		 clock2a : OUT STD_LOGIC;
		 clock3 : OUT STD_LOGIC;
		 clock4 : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT memoria_datos
	PORT(we : IN STD_LOGIC;
		 inclock : IN STD_LOGIC;
		 outclock : IN STD_LOGIC;
		 address : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
		 data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 q : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT buf
	PORT(enable : IN STD_LOGIC;
		 bus_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 bus_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT pic_alu
	PORT(carry_in : IN STD_LOGIC;
		 a : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 b : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 operation : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		 carry_out : OUT STD_LOGIC;
		 zero : OUT STD_LOGIC;
		 match : OUT STD_LOGIC;
		 res : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_to_bcd
	PORT(clock : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 enable_in : IN STD_LOGIC;
		 centena : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 decena : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 unidad : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 to_bcd : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT pic_ctrl
	PORT(match : IN STD_LOGIC;
		 file_addr : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
		 inst : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
		 w_wen : OUT STD_LOGIC;
		 w_a_oen : OUT STD_LOGIC;
		 w_b_oen : OUT STD_LOGIC;
		 pc_wen : OUT STD_LOGIC;
		 pc_oen : OUT STD_LOGIC;
		 pclath_wen : OUT STD_LOGIC;
		 pclath_oen : OUT STD_LOGIC;
		 pc_push : OUT STD_LOGIC;
		 pc_pop : OUT STD_LOGIC;
		 pc_load : OUT STD_LOGIC;
		 fsr_wen : OUT STD_LOGIC;
		 fsr_oen : OUT STD_LOGIC;
		 file_wen : OUT STD_LOGIC;
		 file_oen : OUT STD_LOGIC;
		 pc_skip : OUT STD_LOGIC;
		 imm_oen : OUT STD_LOGIC;
		 status_oen : OUT STD_LOGIC;
		 status_wen : OUT STD_LOGIC;
		 carry_wen : OUT STD_LOGIC;
		 zero_wen : OUT STD_LOGIC;
		 const_oen : OUT STD_LOGIC;
		 const_01 : OUT STD_LOGIC;
		 alu_op : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_cons
	PORT(out_en : IN STD_LOGIC;
		 const_01 : IN STD_LOGIC;
		 data_out : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_fsr
	PORT(clock_4 : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 out_en : IN STD_LOGIC;
		 write_en : IN STD_LOGIC;
		 data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 fsr_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_inst
	PORT(clock_1 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 out_en : IN STD_LOGIC;
		 inst_in : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
		 imm_out : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 inst_7 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		 inst_8 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 inst_out : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_pc
	PORT(clock_2 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 out_en : IN STD_LOGIC;
		 write_en : IN STD_LOGIC;
		 push : IN STD_LOGIC;
		 pop : IN STD_LOGIC;
		 load : IN STD_LOGIC;
		 pc_skip : IN STD_LOGIC;
		 addr_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 stack_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 we : OUT STD_LOGIC;
		 addr_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 stack_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 stack_ptr : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_s
	PORT(clock_4 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 out_en : IN STD_LOGIC;
		 write_en : IN STD_LOGIC;
		 carry_in : IN STD_LOGIC;
		 zero_in : IN STD_LOGIC;
		 carry_wr : IN STD_LOGIC;
		 zero_wr : IN STD_LOGIC;
		 data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 carry_out : OUT STD_LOGIC;
		 banking : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		 data_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg_w
	PORT(clock_4 : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 out_a_en : IN STD_LOGIC;
		 out_b_en : IN STD_LOGIC;
		 write_en : IN STD_LOGIC;
		 data_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_a_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 data_b_out : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 valor_wr : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT vgaInterface
	PORT(clock : in  STD_LOGIC;
		instruction : in STD_LOGIC_VECTOR (13 downto 0);
		char : out  STD_LOGIC_VECTOR (7 downto 0)
	);
END COMPONENT;

SIGNAL	clock25in :  STD_LOGIC;
SIGNAL	bus_a :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	bus_b :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	Clock1_line :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC_VECTOR(6 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_65 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_5 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_66 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_7 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_8 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_9 :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_10 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_11 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_12 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_13 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_14 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_15 :  STD_LOGIC_VECTOR(0 TO 13);
SIGNAL	SYNTHESIZED_WIRE_67 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_17 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_18 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_68 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_21 :  STD_LOGIC_VECTOR(8 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_23 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_24 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_25 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_26 :  STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_27 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_28 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_29 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_30 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_31 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_32 :  STD_LOGIC_VECTOR(8 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_33 :  STD_LOGIC_VECTOR(13 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_34 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_35 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_37 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_38 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_40 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_41 :  STD_LOGIC_VECTOR(13 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_42 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_43 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_44 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_45 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_46 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_47 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_48 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_49 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_51 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_53 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_54 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_55 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_56 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_57 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_58 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_61 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_62 :  STD_LOGIC;

signal ASCII_CHAR: STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN 
SYNTHESIZED_WIRE_11 <= '0';
SYNTHESIZED_WIRE_15 <= "00000000000000";

-- synthesis translate_off 
c0   <= SYNTHESIZED_WIRE_13; 			 -- Program Memory OUT Clock
c1   <= Clock1_line;						 --  Instruction Register
c2   <= SYNTHESIZED_WIRE_42;  		 -- Program Counter Register
c2a  <= SYNTHESIZED_WIRE_12;  		 -- Program Memory IN Clock
c3   <= SYNTHESIZED_WIRE_7;			 -- Stack Register Clock
c4   <= SYNTHESIZED_WIRE_65;  		 -- Others Clock (wr fsr etc)
inst <= SYNTHESIZED_WIRE_33; 			 -- 41 no ha sido filtrada (skips)
program_counter <= SYNTHESIZED_WIRE_14; -- adress to program memory
-- synthesis translate_on 

b2v_inst : fadr_mux
PORT MAP(bank_irp => SYNTHESIZED_WIRE_0,
		 fsr => SYNTHESIZED_WIRE_1,
		 inst_7 => SYNTHESIZED_WIRE_2,
		 data_addr => SYNTHESIZED_WIRE_21,
		 file_addr => SYNTHESIZED_WIRE_32);


b2v_inst1 : pclath
PORT MAP(clock_4 => SYNTHESIZED_WIRE_65,
		 reset => reset,
		 out_en => SYNTHESIZED_WIRE_4,
		 write_en => SYNTHESIZED_WIRE_5,
		 data_in => SYNTHESIZED_WIRE_66,
		 data_out => bus_a);


b2v_inst10 : stack_reg
PORT MAP(clock_3 => SYNTHESIZED_WIRE_7,
		 reset => reset,
		 we => SYNTHESIZED_WIRE_8,
		 address => SYNTHESIZED_WIRE_9,
		 write_data => SYNTHESIZED_WIRE_10,
		 read_data => SYNTHESIZED_WIRE_51);


b2v_inst11 : memoria_programa
PORT MAP(we => SYNTHESIZED_WIRE_11,
		 inclock => SYNTHESIZED_WIRE_12,
		 outclock => SYNTHESIZED_WIRE_13,
		 address => SYNTHESIZED_WIRE_14,
		 data => SYNTHESIZED_WIRE_15,
		 q => SYNTHESIZED_WIRE_41);


b2v_inst12 : digit_s
PORT MAP(rst => reset,
		 from_wr_we => SYNTHESIZED_WIRE_67,
		 clk => clock25in,
		 eight_prl => SYNTHESIZED_WIRE_17,
		 modout => carry_out_centena,
		 store => SYNTHESIZED_WIRE_27,
		 qdigit_c => SYNTHESIZED_WIRE_28,
		 qdigit_d => SYNTHESIZED_WIRE_29,
		 qdigit_u => SYNTHESIZED_WIRE_30);




b2v_inst15 : clock_gen
PORT MAP(reset => reset,
		 clock => clock25in,
		 clock_l => SYNTHESIZED_WIRE_68,
		 clock0 => SYNTHESIZED_WIRE_13,
		 clock1 => Clock1_line,
		 clock2 => SYNTHESIZED_WIRE_42,
		 clock2a => SYNTHESIZED_WIRE_12,
		 clock3 => SYNTHESIZED_WIRE_7,
		 clock4 => SYNTHESIZED_WIRE_65);


b2v_inst16 : memoria_datos
PORT MAP(we => SYNTHESIZED_WIRE_18,
		 inclock => SYNTHESIZED_WIRE_68,
		 outclock => SYNTHESIZED_WIRE_68,
		 address => SYNTHESIZED_WIRE_21,
		 data => SYNTHESIZED_WIRE_66,
		 q => SYNTHESIZED_WIRE_24);


b2v_inst17 : buf
PORT MAP(enable => SYNTHESIZED_WIRE_23,
		 bus_in => SYNTHESIZED_WIRE_24,
		 bus_out => bus_a);


b2v_inst2 : pic_alu
PORT MAP(carry_in => SYNTHESIZED_WIRE_25,
		 a => bus_a,
		 b => bus_b,
		 operation => SYNTHESIZED_WIRE_26,
		 carry_out => SYNTHESIZED_WIRE_55,
		 zero => SYNTHESIZED_WIRE_56,
		 match => SYNTHESIZED_WIRE_31,
		 res => SYNTHESIZED_WIRE_66);


b2v_inst20 : reg_to_bcd
PORT MAP(clock => clock25in,
		 rst => reset,
		 enable_in => SYNTHESIZED_WIRE_27,
		 centena => SYNTHESIZED_WIRE_28,
		 decena => SYNTHESIZED_WIRE_29,
		 unidad => SYNTHESIZED_WIRE_30,
		 to_bcd => bcd_port);


b2v_inst3 : pic_ctrl
PORT MAP(match => SYNTHESIZED_WIRE_31,
		 file_addr => SYNTHESIZED_WIRE_32,
		 inst => SYNTHESIZED_WIRE_33,
		 w_wen => SYNTHESIZED_WIRE_67,
		 w_a_oen => SYNTHESIZED_WIRE_61,
		 w_b_oen => SYNTHESIZED_WIRE_62,
		 pc_wen => SYNTHESIZED_WIRE_44,
		 pc_oen => SYNTHESIZED_WIRE_43,
		 pclath_wen => SYNTHESIZED_WIRE_5,
		 pclath_oen => SYNTHESIZED_WIRE_4,
		 pc_push => SYNTHESIZED_WIRE_45,
		 pc_pop => SYNTHESIZED_WIRE_46,
		 pc_load => SYNTHESIZED_WIRE_47,
		 fsr_wen => SYNTHESIZED_WIRE_38,
		 fsr_oen => SYNTHESIZED_WIRE_37,
		 file_wen => SYNTHESIZED_WIRE_18,
		 file_oen => SYNTHESIZED_WIRE_23,
		 pc_skip => SYNTHESIZED_WIRE_48,
		 imm_oen => SYNTHESIZED_WIRE_40,
		 status_oen => SYNTHESIZED_WIRE_53,
		 status_wen => SYNTHESIZED_WIRE_54,
		 carry_wen => SYNTHESIZED_WIRE_57,
		 zero_wen => SYNTHESIZED_WIRE_58,
		 const_oen => SYNTHESIZED_WIRE_34,
		 const_01 => SYNTHESIZED_WIRE_35,
		 alu_op => SYNTHESIZED_WIRE_26);


b2v_inst4 : reg_cons
PORT MAP(out_en => SYNTHESIZED_WIRE_34,
		 const_01 => SYNTHESIZED_WIRE_35,
		 data_out => bus_b);


b2v_inst5 : reg_fsr
PORT MAP(clock_4 => SYNTHESIZED_WIRE_65,
		 rst => reset,
		 out_en => SYNTHESIZED_WIRE_37,
		 write_en => SYNTHESIZED_WIRE_38,
		 data_in => SYNTHESIZED_WIRE_66,
		 data_out => bus_a,
		 fsr_out => SYNTHESIZED_WIRE_1);


b2v_inst6 : reg_inst
PORT MAP(clock_1 => Clock1_line,
		 reset => reset,
		 out_en => SYNTHESIZED_WIRE_40,
		 inst_in => SYNTHESIZED_WIRE_41,
		 imm_out => bus_b,
		 inst_7 => SYNTHESIZED_WIRE_2,
		 inst_8 => SYNTHESIZED_WIRE_49,
		 inst_out => SYNTHESIZED_WIRE_33);


b2v_inst7 : reg_pc
PORT MAP(clock_2 => SYNTHESIZED_WIRE_42,
		 reset => reset,
		 out_en => SYNTHESIZED_WIRE_43,
		 write_en => SYNTHESIZED_WIRE_44,
		 push => SYNTHESIZED_WIRE_45,
		 pop => SYNTHESIZED_WIRE_46,
		 load => SYNTHESIZED_WIRE_47,
		 pc_skip => SYNTHESIZED_WIRE_48,
		 addr_in => SYNTHESIZED_WIRE_49,
		 data_in => SYNTHESIZED_WIRE_66,
		 stack_in => SYNTHESIZED_WIRE_51,
		 we => SYNTHESIZED_WIRE_8,
		 addr_out => SYNTHESIZED_WIRE_14,
		 data_out => bus_a,
		 stack_out => SYNTHESIZED_WIRE_10,
		 stack_ptr => SYNTHESIZED_WIRE_9);


b2v_inst8 : reg_s
PORT MAP(clock_4 => SYNTHESIZED_WIRE_65,
		 reset => reset,
		 out_en => SYNTHESIZED_WIRE_53,
		 write_en => SYNTHESIZED_WIRE_54,
		 carry_in => SYNTHESIZED_WIRE_55,
		 zero_in => SYNTHESIZED_WIRE_56,
		 carry_wr => SYNTHESIZED_WIRE_57,
		 zero_wr => SYNTHESIZED_WIRE_58,
		 data_in => SYNTHESIZED_WIRE_66,
		 carry_out => SYNTHESIZED_WIRE_25,
		 banking => SYNTHESIZED_WIRE_0,
		 data_out => bus_a);


b2v_inst9 : reg_w
PORT MAP(clock_4 => SYNTHESIZED_WIRE_65,
		 rst => reset,
		 out_a_en => SYNTHESIZED_WIRE_61,
		 out_b_en => SYNTHESIZED_WIRE_62,
		 write_en => SYNTHESIZED_WIRE_67,
		 data_in => SYNTHESIZED_WIRE_66,
		 data_a_out => bus_a,
		 data_b_out => bus_b,
		 valor_wr => SYNTHESIZED_WIRE_17);

b2v_inst21 : buf
PORT MAP(enable => '1',
		 bus_in => SYNTHESIZED_WIRE_17,
		 bus_out => wr_out);
		 
Inst_scale_clock: scale_clock PORT MAP(
		clk_50Mhz => clock50in,
		rst => reset,
		clk_div => clock25in 
	);
	
Inst_lcd: lcd PORT MAP(
		clk => clock50in,
		rst => reset,
		test_lcd => SYNTHESIZED_WIRE_41,
		test_lcd_wr => SYNTHESIZED_WIRE_17,
		SF_D => SF_D,
		LCD_E => LCD_E,
		LCD_RS => LCD_RS,
		LCD_RW => LCD_RW,
		SF_CE0 => SF_CE0
	);
	
Inst_vga_interface : vgaInterface PORT MAP(
		clock => SYNTHESIZED_WIRE_42,
		instruction => SYNTHESIZED_WIRE_33,
		char => ASCII_CHAR);

END bdf_type;