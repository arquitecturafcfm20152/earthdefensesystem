-- "reg_s.vhd"

-- Status register.  Tiene una salida triestado y salidas/entradas
-- dedicadas para el carry y el zero.

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY reg_s IS
    PORT (
           clock_4     : IN  STD_LOGIC;
           reset     : IN  STD_LOGIC;
           out_en    : IN  STD_LOGIC;
           write_en  : IN  STD_LOGIC;
           data_in   : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
           data_out  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
           carry_out : OUT STD_LOGIC;
           carry_in  : IN  STD_LOGIC;
           zero_in   : IN  STD_LOGIC;
           carry_wr  : IN  STD_LOGIC;
           zero_wr   : IN  STD_LOGIC;
  		   banking	 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
         );
END reg_s;

ARCHITECTURE mixed OF reg_s IS

SIGNAL valor: STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN

    data_out 	<= valor WHEN out_en = '1' ELSE "ZZZZZZZZ";
    carry_out 	<= valor(0);    
	 banking 	<= valor(7 downto 5);

    do_it: PROCESS (clock_4, reset)
    BEGIN
        IF reset = '0' THEN
            valor <= "00000000";
        ELSIF clock_4'EVENT AND clock_4 = '1' THEN
            IF write_en = '1' THEN
                valor <= data_in;
            ELSE
                IF carry_wr = '1' THEN     
                    valor(0) <= carry_in;
                END IF;
                IF zero_wr = '1' THEN     
                    valor(2) <= zero_in;
                END IF;
            END IF;
        END IF;
    END PROCESS;
END mixed;
